# Qu'est ce que votre projet ?

### Description succincte du projet

Outil d'aide aux décisions boursières.

### Quel est l'objectif principal de celui-ci ?

Nous voulons faire un outil d'analyse de données récupérées sur des sites d'information, afin de réaliser des prédictions sur l'évolution des cours de la bourse.

# A qui sert-il ?

### Quels sont les différents types d'utilisateurs ?

Nous visons principalement les traders, professionnels comme amateurs.
Notre outil leur donnerait des conseils pour mieux miser en bourse.
Nous pourrions également rendre disponible les données analysées afin que d'autres types d'utilisateurs puissent émerger.

# Pourquoi ce projet ?

### Comment se positionne votre projet par rapport à l'existant ?

La finance boursière est définie actuellement *principalement* par 2 éléments:
- les évènements dans un passé très proche.
- les variations purement financières.
A notre connaissance, il n'y a que les traders humains qui prennent en compte ces deux éléments, ce qui correspond à approximativement 50% de la masse des flux dans la finance boursière.

Notre projet doit mettre en avant les bénéfices à court, moyen et long terme, grâce à un modèle prédictif qui prend en compte les aspects structurels, historiques et humains.

D'après ce que nous savons, ce type de données est peu utilisé dans le trading professionnel.

Concernant le trading dit "amateur", le projet doit permettre d'avoir une approche plus visuelle et logique, qui agrègera l'information purement technique afin de faire ressortir des suggestions et des informations lisibles pour l'humain.

Enfin, nous envisageons sérieusement de rendre open-source une partie de notre projet (la partie prédiction).
Cela nous permettrait d'un coté de potentiellement bénéficier d'aide extérieure plus simplement, mais également rendrait perein notre projet même si, une fois la 5ème année à Epitech terminée, certains decident de le quitter.

### Quels sont les concurrents (Descriptions succinctes) ?

Nos concurrents principaux seront les entreprises d'investissement, comme Betterment, Vanguard, Fidelity, TD Ameritrade ou encore Bogleheads.
Ils ont une très forte capacité d'investissement, mais ont une forte inertie.

### Qu'apporte votre projet ? Y a-t-il un manque ? Un besoin nouveau ?

Notre projet s'inscrit dans un mouvement actuel qui vise à corréler les données via les capacités nouvelles du Big Data.
La possibilité de prévoir précisément les effets d'un évènement, ainsi que sa probabilité d'arriver dans un contexte donné, à partir des données que nous pourrons aggréger, serait enfin un atout considérable pour évaluer une valeur boursière.
Notre projet a pour but de proposer une forme d'analyse qui est à notre connaissance peu exploitée par les algorithmes d'aujourd'hui, l'analyse textuelle.

### A quels problèmes ou besoins votre projet répond-t-il ?

Que le trader soit humain ou automatique, deux problèmatiques se pose toujours: celle de la flexibilité et de la puissance de calcul.
Alors que l'humain n'est pas capable seul de prendre en compte autant de données qu'une centaine de machines tournant à plein régime, un robot n'est pas capable aujourd'hui, dans un contexte de finance boursière, de prendre en compte des facteurs comme les politiques conflictuelles de certains territoires.
Notre objectif est de permettre à la finance de marché de prendre en compte des éléments jusque là traités par des humains avec leur limitations physiques.

### Que feront vos utilisateurs avec votre projet ?

Notre projet permettra aux utilisateurs de connaitre la meilleure décision boursière à prendre, afin qu'ils puissent miser en bourse en optimisant les probabilités.
A terme, une fois que nous aurons optimisé nos algorithmes, nous pourrions améliorer notre projet pour automatiser la prise de décision, et la manipulation de flux financiers.

#  Décrire le résultat

### Comment utiliser votre projet ?

Les utilisateurs pourront aller sur notre site web pour rechercher des conseils et des statistiques pour mieux miser en bourse.
Nous voulons aussi créer une API permettant à un utilisateur de se connecter à la base de données, avant que les données ne soient traitées par l'algorithme.
Ceci afin de développer et permettre à d'autres de proposer ses propres solutions à partir des données disponibles.
Mais après un traitement technique qui nous éviterait tout soucis de copyright sur les données.

### Comment interagir avec le projet ?

Les utilisateurs pourront sélectionner l'entreprise sur laquelle ils veulent des informations. Le logiciel leur fournira différentes données sur l'entreprise, comme leur évolution en bourse, les dernières news les concernant, et une prédiction sur leur évolution future à court, moyen et long terme.
L'objectif est de créer ici une interface simple et classique à un système complexe et nouveau en arrière plan.

### Quelles sont les interactions de votre produit par les utilisateurs ?

Les utilisateurs paieront un abonnement pour accéder aux données de notre site.
Ils pourront y consulter les données humainement lisibles (conseils, statistiques) que nous aurons déduites à l'aide de nos calculs précédents, sur un titre.
L’interaction avec les utilisateurs sera uniquement une lecture de données et n'entrainera pas de calculs.
Notre projet se traduira par un site web sur abonnement, qui servira d'interface avec les utilisateurs et affichera des conseils et les tendances actuelles de la bourse.

### A quoi ressemblera le produit fini ?

Nous souhaitons diviser notre travail en 4 grands pôles.
1. Le Scrap: la récupération du contenu des sources (informations de type statistiques, textuelles, ...)
2. L'analyse technique: les textes sont analysés syntaxiquement, pondérés, afin d'être exploitables pour les prochaines étapes.
3. L'analyse profonde: les sources seront évaluées, sur plusieurs couches de calculs qui permettront une élévation du recoupement des données. Il s'agit également des calculs prédictifs.
4. La présentation des données: Les données sont mises en forme pour être lisibles par des humains.

L'architecture en arrière-guichet (back-office) (1, 2, 3) permettra de distribuer l'ensemble des calculs et la récupération d'informations dans un ensemble de tâches asynchrones.
Le scraping (1) alimentera les algorithmes de calcul (2, 3).
Le guichet (front-office) (4) permettra de présenter les résultats.

### Sur quoi cela fonctionnera-t-il ?

Le scraping se présentera sous forme de scripts capables d'explorer les api, pages web, flux rss. Ils seront capables d'identifier des segments de données (articles, twitts, ...) et de les extraire.

Notre projet comportera une base de données installée sur un serveur distribué afin de faire face à la masse potentielle des données.
Le but est d'être flexible et générique afin de ne pas avoir à recoder les interactions avec la base de donnée une fois que la masse de donnée sera trop grande pour un serveur de bases de données relationnelles.
Cela permettra également de distribuer les calculs de manière appropriée. Nous n'avons pas encore fait notre choix définitif, mais nous étudions les possibilités connues et documentées (Spark et Hadoop).

Les données présentables seront installées sur un serveur de bases de données relationelles conventionnel afin d'être présentable facilement sur le guichet.

# Moyens : avec quoi ?

### Quels moyens humains ?

Notre groupe se compose de 10 personnes.
Parmi elles, certaines se sont spécialisées dans le scraping et l'architecture réseau.
Nous avons également dans notre équipe une comptable de formation, ainsi qu'un ancien trader étudiant.

Plusieurs d'entre nous seront assignés à plusieurs pôles en même temps, afin de permettre à chacun de donner son avis et de participer à la recherche sur les sujets les plus complexe, tout en développant les sujets les plus lourds.

La table suivante représente les pôles principaux auxquels nous participerons activement.

-- Tâches de fond --
> Architecture (Scraping)
- Arthur Poulet - Créer un système performant
- Thibaut Broggi - Créer un système performant
- Arthur Amstutz - Trouver des catégories de données
- Raphael Elkaim - Trouver des catégories de données

> Recherche de sources (Scraping)
- Frédéric Ledarath
- Antoine Prevost

> Front end (SQL, Dataviz)
- Emilie Dezeriaud

> Administration Système / Infrastructure / Réseau
- Alexandre Kalatzis

> Validation financière (data, algos)
- Antoine Prevost
- Emilie Dezeriaud

-- Tâche expérimentales --
> Base de données décentralisée
- Alexandre Kalatzis

> Algo prédictifs
- Arthur Poulet
- Thibaut Broggi
- Lucie Dispot
- Ludovic Douziech

> Sémantique (Text Mining)
- Lucie Dispot (Responsable)
- Ludovic Douziech
- Arthur Amstutz
- Raphael Elkaim

### Des idées de partenaire ?

Bloomberg nous a été recommandé, nous n'avons pas encore pris contact.
Afin d'avoir du matériel (bases de données, etc.), nous comptons sur les partenariats du lab eip avec des entreprises telles qu'Amazon AWS, Microsoft, ...

### De quels moyens matériels auriez-vous besoin ?

Dans un premier temps, nous souhaitons limiter la masse de données afin d'éviter d'avoir des coûts importants.
Une fois que nos premiers jets permettront d'obtenir quelques résulats, nous étendrons la masse de données, ce qui augmentera les coûts.

Notre projet reposera essentiellement sur des serveurs distribués, car nous avons besoin d'une puissance et d'un espace de stockage adaptables et massifs.
- Le cout des scapers sera très faible (entre 0 et 10€ par mois)
- Le cout du stockage n'est pas encore estimé, nous pensons qu'il s'agira de notre dépense la plus importante.
Afin d'optimiser les couts afin de créer notre prototype, nous pensons que 100€ de budget par mois nous permettra d'avoir d'espace et la capacité de calcul suffisante.

### Quels sont les logiciels dont vous auriez besoin ?

Nous utiliserons Docker, qui nous permettra de modulariser le projet.
Nous utiliserons également une base de données NoSQL en utilisant un Framework de calcul décentralisé (Hadoop à priori).
Nous utiliserons des langages et des supports libres afin de développer les logiciels nécessaires.

Enfin, nous utiliserons des outils de gestion de projets tels que Rocket.chat (clone de slack), Atlassian, ...

Il n'est pas pertinent de se poser la question du langage dès maintenant, car les principaux prolèmes techniques auxquels nous devrons répondre concerneront les bases de données et l'analyse sémantique.
Nous nous pencherons surement sur des langages fonctionels tels que Elixir/Erlang, ou Scala.

Nous utiliserons une architecture par API pour les scrap, cela nous permet d'éviter les problèmatiques de langages sur cette partie, bien que le Ruby sera certainement utilisé.

# Comment vous le fabriquez ?

### Qu'est ce que vous développez ?

Les programmes d'analyse de données seront développés dans un langage adapté à leur fonction (sémantique, crawling, ...) et exécutés dans des Dockers, reliés eux aussi aux serveurs distribués.

La base de données décentralisée sera basée sur des serveurs distribués sur un cloud, et reliera toutes les fonctionnalités du projet entre elles. Nous pensons utiliser pour cela le framework Hadoop très répandu.

Notre projet comportera une interface utilisateur sous la forme d'un site web.
Ce site web sera relié à un serveur SQL, qui traitera les requêtes du client et servira de liaison entre le site et la base de données. Cette base SQL contiendra les données visuelles et exploitables.

# Quand ?

### Quel est votre planning ?

-- Mai 2016 --
Il nous faudra tout d'abord définir une architecture basique pour stocker les données à moindre coût financier, ainsi qu'un cahier des charges complet.

-- Août 2016 --
Nous souhaitons dans un premier temps nous limiter à un domaine spécifique (l'informatique) afin de tester nos idées.
Cela permettra également de limiter la masse de données à traiter, donc la durée des calculs et de la récupération des informations.
Pour ce faire, nous récupèrerons en premier temps des données numériques sur des sites spécialisés (presse informatique, presse éconnomique, données publiques).
Nous mettrons très bientôt une liste non-exhaustive et modifiable de ces sites.
Cela sera rapide et permettra de poser l'architecture de scraping.
Nous aurons également à ce moment eu le temps de choisir quelle architecture nous voulons utiliser afin de stocker les données durant leurs différents stades.

-- Octobre 2016 --
Nous mettrons en place la base de données décentralisée, et nous commencerons à l'alimenter.
C'est à dire que nous installerons une architecture asynchrone entre le scraping et la base, ainsi que le premier serveur de calcul.
Nous déploierons sur le serveur de calcul un premier jet d'analyse technique.

-- Decembre 2016 --
Premiers tests d'algorithme prédictif et de présentation de données.
À cet instant, nous aurons toute l'architecture debout, ce qui nous permettra d'avancer sur toutes les dimensions du projet.

-- Juin 2016 --
Nous souhaitons à cette date avoir suffisament fait de recherche pour ouvrir notre base à un maximum de domaines possibles.
Nous avons également pour objectif d'avoir de premiers résultats sur l'analyse syntaxique afin de pouvoir traiter des textes pour alimenter notre base.

-- Octobre 2017 --
Utilisation de plusieurs serveurs afin de distribuer les calculs et augmenter leur vitesse.

-- Janvier 2018 --
L'étape finale consistera à faire de la recherche pour augmenter la pertinence de nos statistiques à partir de machine learning à travers des réseaux de neurones.

# Où ?

### Devez-vous être ou interagir avec des lieux particuliers pour fabriquer votre projet ?

Non.

# Contraintes

### Quelles sont les types de contraintes ?

Une des contraintes principales est le coût de l'infrastructure nécessaire à notre projet: nous aurons besoin de beaucoup de serveurs pour pouvoir stocker toutes les données et les analyser.
C'est pourquoi notre projet part de peu de données, avec un système de serveurs distribués qui peut s'adapter à nos besoins.

La pénétration du milieu financier ne sera sans doutes pas une évidence, mais nous avons déjà commencé à contacter des acteurs extérieurs afin de rentrer dans le domaine.

La récupération de donnée peut-être simple (flux rss, api publiques, ...), compliquée (html, api privées, ...) ou presque impossible (protections anti-bot, ...).
Nous avons dans tous les cas une expérience dans le domaine.
Nous pensons être largement capable, au vu du nombre de sites disponibles, de récupérer suffisament de données pour que quelques sites ne soient pas bloquants.

Enfin, nous avons aujourd'hui peu d'expérience en Big Data, Analyse Syntaxique et Machine Learning. C'est la raison pour laquelle nous avons un grand nombre d'expérimentations à faire avant d'être certains que nos résultats seront utilisables.

# Périmètre de votre projet

### Quels sont les chiffres associés à votre projet ?

Nous aurons un besoin exponentiel de serveurs. Nous avons l'intention de commencer par un seul serveur, et pensons évoluer jusqu'à une quarantaine de serveurs d'ici 2018.

# Cadre légal de votre projet

### Vérifier les aspects légaux : données utilisateurs, droits d'auteurs, contrefaçons... Voyez-vous un potentiel problème ?

Nous ne récupèrerons que des données publiques, que nous ne redistribuerons pas. Nous nous contenterons de donner les résultats de nos analyses, ce qui est légal en France, et à notre connaissance en Europe.

# Cadre du projet

### Votre projet est-il une reprise ou contribution à un projet existant, un projet d'entreprise (création ou commandité...) ?

Notre projet est original et de notre seule initiative. C'est donc notre création.
