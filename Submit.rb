#!/usr/bin/env ruby
#coding: utf-8

require 'pry'
require 'capybara/poltergeist'
require 'nokogiri'

Capybara.register_driver :poltergeist do |app|
  Capybara::Poltergeist::Driver.new(app, js_errors: false)
end
Capybara.default_driver = :poltergeist

module EIP
  module Workflow

    class Submiter
      attr_reader :agent
      attr_writer :login, :pass

      def self.get_content f
        c = File.read(f).gsub(/^#[^#].+\n\n/, '')
        text = c.split(/###.+\n\n/).map{|e| e.strip}.select{|e| not e.empty? }
        Hash[PLACEHOLDERS.zip(text)]
      end

      def initialize login, pass, name, file_update="submit.md"
        @login = login
        @pass = pass
        @name = name
        @agent = Capybara.current_session
        @content = Submiter.get_content(file_update)
        connect
        while @agent.html.include? "<!-- Login form -->"
          connect
          sleep 0.5
        end
      end

      def update
        puts "Update!"
        goto_workflow
        change_workflow
      end

      private
      public
      def connect
        puts "Try to connect !"
        visit "https://eip.epitech.eu"
        @agent.find_field("login").set @login
        @agent.find_field("password").set @pass
        puts "Connection !"
        @agent.find_button("Se connecter").click
        sleep 0.5
        # TODO: check if on the right page
      end

      def goto_workflow
        puts "Go to Workflow !"
        visit("https://eip.epitech.eu/#/users/user/#{@login}")
        @agent.find_link(@name).click
        @agent.find_button("Modifier le workflow").click
      end

      def change_workflow
        puts "Change the workflow !"
        @content.each do |select, content|
          @agent.find_field(select).set(content)
        end
        @agent.find_button("Mettre à jour le workflow").click
      end

      def visit url
        @agent.visit(url)
        sleep 0.5
      end

      PLACEHOLDERS = ["Description succincte du projet",
 "Quel est l'objectif principal de celui-ci ?",
 "Quels sont les différents types d'utilisateurs ?",
 "Comment se positionne votre projet par rapport à l'existant ?",
 "Quels sont les concurrents (Descriptions succinctes) ?",
 "Qu'apporte votre projet ? Y a-t-il un manque ? Un besoin nouveau ?",
 "A quels problèmes ou besoins votre projet répond-t-il ?",
 "Que feront vos utilisateurs avec votre projet ?",
 "Comment utiliser votre projet ?",
 "Comment interagir avec le projet ?",
 "Quelles sont les interactions de votre produit par les utilisateurs ?",
 "A quoi ressemblera le produit fini ?",
 "Sur quoi cela fonctionnera-t-il ?",
 "Quels moyens humains ?",
 "Des idées de partenaire ?",
 "De quels moyens matériels auriez-vous besoin ?",
 "Quels sont les logiciels dont vous auriez besoin ?",
 "Qu'est ce que vous développez ?",
 "Quel est votre planning ?",
 "Devez-vous être ou interagir avec des lieux particuliers pour fabriquer votre projet ?",
 "Quelles sont les types de contraintes ?",
 "Quels sont les chiffres associés à votre projet ?",
 "Vérifier les aspects légaux : données utilisateurs, droits d'auteurs, contrefaçons... Voyez-vous un potentiel problème ?",
 "Votre projet est-il une reprise ou contribution à un projet existant, un projet d'entreprise (création ou commandité...) ?"]

    end

  end
end

if __FILE__ == $0
  binding.pry
  raise "set EIP_WORKFLOW_LOGIN env variable" unless login = ENV["EIP_WORKFLOW_LOGIN"] # login
  raise "set EIP_WORKFLOW_PASS env variable" unless pass = ENV["EIP_WORKFLOW_PASS"] # pass
  raise "set EIP_WORKFLOW_NAME env variable" unless name = ENV["EIP_WORKFLOW_NAME"] # workflow name
  begin
    puts "Arguments are : (#{login}) (#{pass[0..1]}...#{pass[-2..-1]}) (#{name})"
    submiter = EIP::Workflow::Submiter.new login, pass, name
    submiter.update
  rescue => err
    puts err
    binding.pry
  end
end
